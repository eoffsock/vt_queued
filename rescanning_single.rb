require_relative 'lib/virustotal_rescan_service'
require_relative 'lib/virustotal_cache_service.rb'
require_relative 'lib/virustotal_oldness_wo_api.rb'

$api_key = '396d790c5df5bdfb69ebd51785cd9e36d0609a68000d0403d37b0fac2a144056'

class VirustotalRescanningGeneric
  POLL_INTERVAL = 10
  MAX_OLDNESS = 100
  MAX_POLL_ATTEMPTS = 3
  def initialize(logger, api_key)
    @vtrs = VirustotalRescanService.new(api_key)
    @vtcs = VirustotalCacheService.new(api_key)
    @vtos = VirustotalOldnessWoAPI.new
    @api_key = api_key
    @logger = logger
    @api_requests_count = 0
  end
end

class VirustotalBulkRescanningService < VirustotalRescanningGeneric
  def initialize(logger, api_key)
    raise NotImplementedError
  end
  def response(domains)
    raise NotImplementedError
  end
end

class VirustotalRescanningReport < VirustotalRescanningGeneric
  attr_reader :r_rescan, :r_cache, 
              :scan_time_server, :scan_time_client, 
              :cache_time_server, :cache_time_client, 
              :vtrs, :vtcs
  def ask_rescan(domain)
    @api_requests_count += 1
    @logger.debug "API REQUESTS: #{@api_requests_count}"
    @r_rescan = @vtrs.response(domain)
    @rescan_time_client = Time.now.utc
    @rescan_time_server = Time.parse(@r_rescan["scan_date"]+" +0000").utc
    @permalink = @r_rescan["permalink"]
  end
  def check_cache_oldness_wo_api
    oldness = @vtos.oldness(@permalink)
    @logger.debug "Checking cache oldness without API, got: #{oldness}"
    oldness
  end
  def ask_cache(domain)
    @api_requests_count += 1
    @logger.debug "API REQUESTS: #{@api_requests_count}"
    #@r_rescan = @vtrs.response(domain)
    @r_cache = @vtcs.response(domain)
    @cache_time_client = Time.now.utc
    @cache_time_server = Time.parse(@r_cache[:hidden][:scan_date]+" +0000").utc
    @permalink = @r_cache["permalink"]
  end
  def rescan_time_oldness
    @rescan_time_client - @rescan_time_server
  end
  def cache_time_oldness
    @cache_time_client - @cache_time_server
  end
  def is_cache_too_old?
    cache_time_oldness > MAX_OLDNESS
  end
  def is_rescan_too_old?
    rescan_time_oldness > MAX_OLDNESS
  end
  def give_actual_report(domain)
    # Cost: 1 API
    ask_cache(domain)
    @logger.info "Cache oldness is #{cache_time_oldness}"
    if is_cache_too_old?
      ask_rescan(domain)
      if is_rescan_too_old?
        @logger.info "VT refused rescan"
        @logger.error "VT refused rescan. It is still #{rescan_time_oldness} seconds old"
      else
        r = poll_for_newer_cache(domain, cache_time_oldness)
        if r
          @logger.info "Got newer result. Oldness is: #{cache_time_oldness}"
        else
          @logger.error "Could NOT got a newer result. Oldness is: #{cache_time_oldness}"
        end
      end
    else
      @r_cache
    end
  end
  def poll_for_newer_cache(domain, start_oldness)
    @logger.debug "Starting poll. Cache oldness is: #{start_oldness}"
    attempt = 0
    while attempt < MAX_POLL_ATTEMPTS
      #TODO: consider moving this at the end of while cycle
      #      if there is no need to save API requests to reduce time spent
      sleep POLL_INTERVAL
      begin
        oldness = check_cache_oldness_wo_api
        @logger.debug "Attempt #{attempt}. Cache oldness is: #{oldness}"
        if oldness < start_oldness
          ask_cache(domain)
          return r_cache
        end
      rescue RuntimeError => e
        @logger.warn "#{e}"
      end
      attempt += 1
    end
    nil
  end
end

# a clever trick of mine which allows to detect
# if script was executed directly with ruby of pry
if ['(pry)', $PROGRAM_NAME].include?(__FILE__)

  logger = Logger.new(STDOUT)
  logger.level = Logger::DEBUG

  api_key = $api_key

  domain = File.read('sample/websites_all.txt').split("\n").shuffle.first

  s = VirustotalRescanningReport.new(logger, api_key)
  r = s.give_actual_report(domain)

  binding.pry

end

