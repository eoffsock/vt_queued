#!/bin/bash

WORKERS=("vt_worker1" "vt_worker2" "vt_worker3")

up=0

for worker in "${WORKERS[@]}"
do
  daemon --running -n $worker && up=$((up+1))
done

echo $up
