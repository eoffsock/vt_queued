require 'logger'
require_relative "vt_real.rb"
require_relative "rabbit_general.rb"

class FakeChannel
  def ack(delivery_info)
    $logger.info "ACK: #{delivery_info.delivery_tag}"
  end
  def reject(delivery_info, _)
    $logger.info "NAK: #{delivery_info.delivery_tag}"
  end
end

class FakeDelivery
  def initialize(tag)
    @tag = tag
  end
  def delivery_tag
    @tag
  end
end

class RabbitMessageTest < RabbitMessage
  def check_vt_result
    return "error" unless @vt_result
    return "error" unless @vt_result.is_a?(Hash)
    #return "error" if @vt_result.has_key?(:error)
    return "error" unless @vt_result.has_key?(:scanners)
    return "error" unless @vt_result[:scanners].count > 0
    #binding.pry unless @vt_result.has_key?(:error)
    "success"
  end
  def status
    r = check_vt_result
    #binding.pry if r == "error"
    r
  end
end

class VtTester
  def initialize
    #@alexa = File.read('test/alexa-top-1m.csv').split("\n").map { |l| l.split(',').last }
    #@sample = File.read('test/studios.list').split("\n")
    @sample = ['ya.ru']
    @vt = VirustotalRequester.new
    @ch = FakeChannel.new
    @task_id = 0
    @pos = 0
  end
  def shuffle
    @sample.shuffle!
    @pos = 0
  end
  def test(portion_size)
    @test_result = { "success": 0, "error": 0 }
    msgs = portion(portion_size)
    msgs = vt_send(msgs)
    msgs.each {|m| @test_result[m.status.to_sym] += 1}
    @test_result
  end
  def portion(size)
    r = @sample[@pos, size].map do |site|
      payload_in = {"website": site, "task_id": @task_id}.to_json
      #TODO: choose the proper use of it
      @task_id += 1
      RabbitMessageTest.new(payload_in, nil, @ch)
    end
    @pos += size
    r
  end
  def vt_send(msgs)
    msgs = @vt.request("TEST", msgs)
    msgs.each do |msg|
      $logger.info "#{msg.website}: #{msg.status}"
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  $logger = Logger.new(STDOUT)
  $logger.level=Logger::DEBUG

  overall = {success: 0, error: 0}
  tester = VtTester.new

  #while true do
    r = tester.test(1)
    success, error = r.values
    overall[:success] += success
    overall[:error] += error
    puts "Test results: #{success}/#{error}"
    success, error = overall.values
    puts "Overall results: #{success}/#{error}"
    puts "Waiting..."
    sleep 15
  #end
end
