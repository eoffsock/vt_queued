class JobManager

  def initialize
    @db = Sequel.connect(:adapter => 'mysql2', :user => 'vt_queue', :host => 'localhost', :database => 'vt_queue',:password=>'itSOs9wUwCceKX8eVUKbmBMFPO8kbetaSQOct8UIBGG9V')
    @ds = @db[:jobs]
  end

  def all
    @ds
  end

  def to_do
    # unused
    @ds.where(state: "new")
  end

  def job(id)
    @ds.where(id: id).first
  end

  def insert(website)
    # new task
    @ds.insert(website: website, state: "new")
  end

  def process(id)
    raise "Should not be used"
    # process one task (unused)
    return false if new_state(id, 'running')
    task = job(id)
    response = VirustotalCacheService.new(task.website)
  end

  def new_state(id, state)
    # change state (unused)
    task = job(id)
    return false unless task
    @ds.where(id: task.id).update(state: state)
  end

  def claim(id)
    # lock one task (unused)
    return false unless job(id)
    @db.transaction do
      new_state(id, "running")
      sleep 20
      new_state(id, "done")
    end
    true
  end

end


