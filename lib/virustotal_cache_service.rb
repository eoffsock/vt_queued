# -*- coding: utf-8 -*-

require "rest-client"

class VirustotalCacheService
  # Хочу кое-что пояснить
  # К внешнему API можно относиться с доверием, а можно без доверия
  # Тут - первый случай: я верю, что API всегда вернет мне то, что обещает
  # Иначе можно надолго застрять с валидацией JSON Schema

  # Я задаю константы прямо в классе, но при желании их можно хранить где угодно
  ApiUrl = 'http://www.virustotal.com/vtapi/v2/url/report'
  ScannersList = ['Avira',
                  'BitDefender',
                  'Dr.Web',
                  'ESET',
                  'Fortinet',
                  'G-Data',
                  'Kaspersky',
                  'Sophos',
                  'Spam404',
                  'Tencent']

  # Здесь я считаю, что список проблемных результатов приходит нам сразу в downcase
  # Если это не так, то я приведу его к такому виду, чтобы избежать плясок с string case-insensitive comparsion
  # Хотя это возможно например так (и это быстрее Regexp и downarray на каждый поиск)
  # ProblemResultsList.any? { |s| s.casecmp(string) == 0 }
  ProblemResultsList = ['malware site',
                        'malicious site',
                        'phishing site']

  def initialize(api_key)
    RestClient.proxy = ENV['http_proxy'] if ENV.has_key?('http_proxy')
    @api_key = api_key
  end

  def response(domain)
    # оставляем возможность передать bulk-запрос, домены разделены символом переноса
    @url = domain.split("\n").map{|x| "http://#{x}"}.join("\n")
    res = RestClient::Request.execute(method: :post, url: ApiUrl, payload: {apikey: @api_key, resource: @url})
    case res.code
      when 200
        json = JSON.parse(res)
        if json.is_a?(Array)
          if json.size == 1
            return generate_result(json.first)
          else
            # ответ на bulk-запрос
            json.inject([]) {|a, x| a + [generate_result(x)]}
          end
        else
          # ответ на единичный запрос
          generate_result(json)
        end
      when 204
        raise 'VirusTotal API request rate limit quota exceeds'
      else
        raise 'Some error here'
    end
  end

  private

  def generate_result(json)
    url = json['resource']
    scan_date = json['scan_date']
    permalink = json['permalink']

    # В случае, если искомого не найдено - лучше вернуть пустой объект
    # И дальше не разбирать
    return { domain: url, error: "URL \"#{url}\" is not found in VirusTotal"} if json['response_code'] != 1

    # Тут мы сворачиваем изначальное
    # {"ScannerName" => {"detected" => false, "result" => "clean site"}
    # к виду {"ScannerName" => "clean site"}
    scans = json['scans'].inject({}) { |h, (k, v)| h[k] = v['result']; h }

    safe = true
    problems = {}
    trusted = {}
    trusted_blacklist = []

    # Лучше сделать один обход и вытащить все результаты сразу
    # На самом деле даже свертку можно уместить сюда при необходимости
    scans.each do |key, verdict|
      if ProblemResultsList.include?(verdict)
        safe = false
        problems[key] = verdict
      end
      if ScannersList.include?(key)
        trusted[key] = verdict
        trusted_blacklist << key if ProblemResultsList.include?(verdict)
      end
    end

    # И собираем все вместе
    # Теоретически можно было бы сделать всю сборку через Hash#select
    # Но получится громоздко, а выигрыш невелик
    {
        domain: json['resource'],
        authority: 'virustotal_cache',
        scanners: scans,
        vulnerable: !safe,
        problems: problems,
        trusted: trusted,
        blacklisted_by: trusted_blacklist,
        hidden: {
          scan_date: scan_date,
          permalink: permalink
        }
    }
  end

end
