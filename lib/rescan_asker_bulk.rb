require_relative 'virustotal_rescan_service'

class VirustotalRescanAskerBulk
  MAX_PER_REQUEST = 20
  COOLDOWN_TIMER = 15
  def initialize *args
    @vtrs = VirustotalRescanService.new(@api_key)
  end
  def rescan_portion portion
    urls = portion.map do |task|
      next task.strip if task.is_a?(String)
      next task.website if task.is_a?(VtTask)
      nil
    end.compact.join("\n")
    vt_results = @vtrs.response(urls)
    update_tasks_w_vt_result portion, vt_results
    good = portion.select { |task| all_ok?(task.vt_result) }
    bad = portion - good
    [ good, bad, portion ]
  end
  def update_tasks_w_vt_result portion, vt_results
    portion.each do |vt_task|
      vt_result = find_result_by_url vt_results, vt_task.website
      vt_task.vt_result = vt_result
    end
  end
  def find_result_by_url vt_results_in, url
    if vt_results_in.is_a? Array
      vt_results = vt_results_in
    else
      vt_results = [ vt_results_in ]
    end
    vt_results.select {|rr| rr["url"].gsub(/^http:\/\//, '').gsub(/\/$/, '') == url}.first
  end
  def log_portion_stats good, bad, r
    return nil
    bad_string = bad.size > 0 ? ", Bad: #{bad.size} / #{r.size}" : ""
    $log.info "Good: #{good.size} / #{r.size}"+bad_string
  end
  def process_good good
    good.map { |l| process_good_item(l) } 
  end
  def process_bad bad
    bad.map { |l| process_bad_item(l) } 
  end
  def process_portion portion
    $log.debug "Got portion #{portion.size}"
    good, bad, r = rescan_portion portion
    log_portion_stats good, bad, r
    process_good good
    process_bad bad
    sleep COOLDOWN_TIMER
  end
  def all_ok?(response)
    return false unless response.is_a?(Hash)
    code = response.fetch('response_code', nil)
    return false unless code && code == 1
    true
  end
end

