class JobConsumer
  VIRUSTOTAL_QUERY_INTERVAL_SEC = 16
  AMQ_USER="guest"
  AMQ_PASS="guest"
  AMQ_HOST="127.0.0.1"
  AMQ_RESULTS_X = "results"
  AMQ_RESULTS_X_DURABLE = true

  def initialize
    #TODO: last request time must be unix 0
    @last_request = DateTime.now

    conn = Bunny.new(user: AMQ_USER, password: AMQ_PASS, host: AMQ_HOST)
    conn.start
    ch   = conn.create_channel
    # x - исходящая точка обмена для результатов
    x_results    = ch.direct(AMQ_RESULTS_X, durable: AMQ_RESULTS_X_DURABLE)
    # q - входящая очередь для задач, биндится к точке с именем клиента, данному через аргумент ("mailru")
    q    = ch.queue("")

    # точка обмена, из которой будем принимать задачи в очередь
    exchange = ARGV.shift || "mailru"
    # биндим очередь к входящей точке обмена ("mailru"/параметр)
    # при этом будем получать задачи только с routing key "delegation" (очередь должна быть direct)
    q.bind(exchange, routing_key: "delegation")

    puts " [*] Waiting for messages in #{q.name}. To exit press CTRL+C"

  end

  def watch
    while true do
      new = @ds.where(state: "new").where('id > ?', @db_last_id).order(:id)
      if new.count == 0
        sleep POLL_INTERVAL_SEC
        next
      end
      @db_last_id = new.last[:id]
      puts "Found #{new.count} new records in dataset. Last is #{@db_last_id}"
      process_bulk(new)
      sleep POLL_INTERVAL_SEC
    end
  end

  def normalize(url)
    # makes url compatible with VirustotalCacheService response format
    url if url =~ /^https?:\/\//
    "http://#{url}"
  end

  def process_bulk(tasks)
    # process multiple tasks as one bulk
    puts "Begin processing #{tasks.count} items."
    websites_joined = tasks.to_a.map {|t| t[:website]}.sort.uniq.join("\n")
    #tasks_by_website = tasks.to_a.inject({}) {|h, x| website = x[:website]; h[website] = x; h}
    begin
      tasks = tasks.to_a
      # indicate task as running
      tasks.each {|t| update_state(t[:id], "running")}
      puts "Sending request"
      # actually do the job - send bulk request and get bulk response
      response = VirustotalCacheService.new(websites_joined).response
      puts "Got response"
      # make dictonary of responses with domain as key
      response_by_domain = response.inject({}) {|h, x| domain = x[:domain]; h[domain] = x; h}
      tasks.each do |t|
        id = t[:id]
        puts "Processing response for task #{id}"
        begin
          # for each domain in a response dict update the db record state/result
          website = t[:website]
          response = response_by_domain[normalize(website)]
          if response
            update_result(id, response)
            update_state(id, 'done')
          else
            update_state(id, 'failed')
          end
        rescue => e
          puts "FAILED #{id}/#{website}: "+e.inspect
          update_state(id, 'failed')
        end
      end
    rescue ArgumentError => e
      #TODO: mark all pending "running" as new again
      puts e.inspect
      #tasks.each {|t| update_state(t[:id], "new")}
    end
  end

  def job(id)
    @ds.where(id: id).first
  end

  def update_state(id, state)
    task = job(id)
    raise ArgumentError unless task
    @ds.where(id: task[:id]).update(state: state)
  end

  def update_result(id, result)
    task = job(id)
    raise ArgumentError unless task
    @ds.where(id: task[:id]).update(result: result)
  end

end


