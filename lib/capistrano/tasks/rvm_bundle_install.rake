namespace :devops do
   desc "Create gemset and run bundle install"
   task :create_gemset do
      on roles(:all) do |host|
        execute "cd #{deploy_to}/current; /home/ilya.ruby/.rvm/bin/rvm ruby-2.2 exec rvm gemset create virustotal"
        execute "cd #{deploy_to}/current; /home/ilya.ruby/.rvm/bin/rvm ruby-2.2@virustotal exec gem install bundle"
        execute "cd #{deploy_to}/current; /home/ilya.ruby/.rvm/bin/rvm ruby-2.2@virustotal exec bundle install"
      end
   end
end

