namespace :devops do
   desc "Copy config files to host"
   task :copy_config do
      on roles(:all) do |host|
         %w[ config/virustotal.god
             config/rabbitmq.yml
         ].each do |f|
            upload_to = "#{deploy_to}/shared/"
            puts "Uploading to #{upload_to} file #{f}"
            upload! f, upload_to + f
         end
      end
   end
end

