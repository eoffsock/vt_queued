require_relative "vt_general.rb"

class VirustotalMocker < VirustotalGeneral
  MIN_SPEED = 0
  MAX_SPEED = 1
  def process_request(sites)
    # simulate the heavy load
    sleep rand * MAX_SPEED + MIN_SPEED
    return ["OK"] * sites.count
  end  
end
