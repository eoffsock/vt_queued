require_relative "rabbit_general.rb"

class RabbitMocker < RabbitGeneral
  PROBABILITY = 0.5
  MAX_PER_SECOND = 10
  MIN_PER_SECOND = 0
  def process_request
    # generating - a rough formula but it makes it
    number_of_items = @time_elapsed * ( ( PROBABILITY * MAX_PER_SECOND ) + MIN_PER_SECOND)
    number_of_items.round.times.inject([]) {|a, x| a << get_one; a}    
  end
  def get_one
    @msg_count += 1
    return @msg_count
  end
  def get_job_fixed_portion(n)
    #constant fixed amount
    n.times.inject([]) {|a,_| a << get_one}
  end  
end
