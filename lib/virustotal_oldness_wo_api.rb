require 'nokogiri'
require 'rest-client'

class VirustotalOldnessWoAPI
  def initialize
  end
  def get_permalink_page(permalink)
    virusttotal_response = RestClient.get(permalink)
    Nokogiri::HTML(virusttotal_response)
  end
  def scan_date_utc(permalink)
    page = get_permalink_page(permalink)
    scan_date_string = page.title.split("\n").select {|l| l.match /UTC/}.first.match('\d[^ ]+ *\d[^ ]+ *[^ ]+')[0]
    Time.parse(scan_date_string).utc
  end
  def oldness(permalink)
    Time.now - scan_date_utc(permalink)
  end
end
