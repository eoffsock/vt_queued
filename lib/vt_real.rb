require_relative "virustotal_cache_service.rb"
require_relative "vt_general.rb"

class VirustotalRequester < VirustotalGeneral
  def initialize(logger)
    raise ArgumentError.new("Provide an API_KEY environmental variable") unless ENV.has_key?("API_KEY")
    @api_key = ENV["API_KEY"]
    super
  end
  def normalize_proto(url)
    # makes url compatible with VirustotalCacheService response format
    return url if url =~ /^https?:\/\//
    "http://#{url}"
  end
  def process_request(msgs)
    # process multiple tasks as one bulk
    @logger.debug "Begin processing #{msgs.count} items."
    websites_joined = msgs.to_a.map {|m| m.website}.sort.uniq.join("\n")
    begin
      @logger.debug "Sending request"
      # actually do the job - send a bulk request and get the bulk response
      response = VirustotalCacheService.new(@api_key).response(websites_joined)
      @logger.debug "Got response"
      response = response.is_a?(Array) ? response : [response]
      # make dictonary of responses with domain as key
      response_by_domain = response.inject({}) {|h, x| domain = x[:domain]; h[domain] = x; h}
      msgs.each do |msg|
        @logger.debug "Processing response for website #{msg.website}"
        msg.vt_result = response_by_domain.fetch(normalize_proto(msg.website), nil)
      end
    rescue ArgumentError => e
      @logger.fatal "VirustotalRequester FATAL ERROR: #{e.inspect}"
    end
    msgs
  end
end
