require 'time'
require_relative "vt_task"

class RabbitGeneral
  def initialize(logger)
    @logger = logger
    @logger.debug "Initialized logger in Rabbit"
    @msg_count = 0
    @last_msg = Time.now
  end
  def get_job_from_queue
    @time_elapsed = Time.now - @last_msg
    @last_msg = Time.now
    result = process_request
    @logger.debug "### RABBIT: got #{result.count} by #{@time_elapsed} sec"
    result
  end
  def process_request
    # overload it
    raise "undefined!!"
  end
end

