class VirustotalGeneral
  def initialize(logger)
    @logger = logger
    @requesters = {}
  end
  def request(requester, sites)
    if @requesters.has_key? requester
      last_request = @requesters[requester]
      diff = Time.now - last_request
      @logger.debug "### VT: asked with #{sites.count} sites by #{requester} after #{diff} second of idling"
      @requesters[requester] = Time.now
    else
      @logger.debug "### VT: asked with #{sites.count} by #{requester} for the first time"
      @requesters[requester] = Time.now
    end
    process_request(sites)
  end
  def process_request(sites)
  # overload it
    raise "undefined!!"
  end
end
