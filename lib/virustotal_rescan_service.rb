# -*- coding: utf-8 -*-

require "rest-client"

class VirustotalRescanService
  ApiUrl = 'https://www.virustotal.com/vtapi/v2/url/scan'
  def initialize(api_key)
    RestClient.proxy = ENV['http_proxy'] if ENV.has_key?('http_proxy')
    @api_key = api_key
  end

  def response(domain)
    rescan_r = ask_vt_for_rescan(domain)    
    #permalink = rescan_r["permalink"]
    #report = get_report(permalink)
  end

  def ask_vt_for_rescan(domain)
    # оставляем возможность передать bulk-запрос, домены разделены символом переноса
    @url = domain.split("\n").map{|x| "http://#{x}"}.join("\n")
    res = RestClient::Request.execute(method: :post, url: ApiUrl, payload: {apikey: @api_key, url: @url})
    case res.code
      when 200
        json = JSON.parse(res)
        if json.is_a?(Array)
          # ответ на bulk-запрос
          json.inject([]) {|a, x| a + [generate_result(x)]}
        else
          # ответ на единичный запрос
          generate_result(json)
        end
      when 204
        raise 'VirusTotal API request rate limit quota exceeds'
      else
        raise 'Some error here'
    end
  end

  def get_report(report_url)
    RestClient.get report_url, {:accept => :json}
  end

  private

  def generate_result(json)
    return json
  end
end
