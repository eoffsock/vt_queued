class VtTask
  attr_reader :delivery_tag, :website, :payload_in, :task_id
  attr_accessor :vt_result
  def initialize(rabbit, payload_in, delivery_tag)
    @rabbit = rabbit
    @payload_in = payload_in
    @payload = JSON.parse(payload_in)
    @delivery_tag = delivery_tag
    @website = @payload["website"]
    @task_id = @payload["task_id"]
  end
  def found?
    return nil unless result_present?
    return false if @vt_result.has_key?(:error) && @vt_result[:error] =~ /is not found/
    true
  end
  def actual?
    return nil unless oldness
    oldness < MAX_OLDNESS
  end
  def oldness
    return nil unless result_present?
    return nil unless found?
    begin
      scan_date = Time.parse(@vt_result[:hidden][:scan_date]+" +0000").utc
      Time.now.utc - scan_date
    rescue ArgumentError
      nil
    end
  end
  def status
    @vt_result ? "success" : "error"
  end
  def result_present?
    return false if @vt_result.nil? || @vt_result.empty?
    true
  end
  def export_result
    json_result = JSON.dump({
      "status" => status,
      "payload" => JSON.dump(@vt_result),
      "task_id" => @task_id
    })
    @rabbit.publish_result(json_result)
  end
  def accept
    @rabbit.ch.ack(@delivery_tag)
  end
  def reject
    # with requeue = true
    @rabbit.ch.reject(delivery_tag, true)
  end
  def rescan
    #TODO: consider removing it
    raise "Should not be used now"
    #@rabbit.to_rescan(self)
  end
end

