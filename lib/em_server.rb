require 'eventmachine'
require 'thin'

class EmServer
  def self.run(opts)
    EM.run do
      server  = opts[:server] || 'thin'
      host    = opts[:host]   || '0.0.0.0'
      port    = opts[:port]   || '8181'
      web_app = opts[:app]

      unless %w(thin hatetepe goliath).include? server
        raise "Need an EM webserver, but #{server} isn't"
      end

      dispatch = Rack::Builder.app do
        map '/' do
          run web_app
        end
      end

      Rack::Server.start(
        app: dispatch,
        server: server,
        Host: host,
        Port: port,
        signals: false,
      )
    end
  end
end