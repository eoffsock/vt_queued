require_relative "rabbit_general.rb"
require 'bunny'
require 'yaml'

class RabbitRequester < RabbitGeneral
  AMQ_USER="guest"
  AMQ_PASS="guest"
  AMQ_HOST="127.0.0.1"
  AMQ_RESULTS_X = "results"
  AMQ_RESULTS_X_DURABLE = true
  AMQ_RESCAN_X = "vt_rescan"
  AMQ_RESCAN_X_DURABLE = true
  AMQ_QUEUE_IN = "virustotal"
  AMQ_QUEUE_IN_DURABLE = true
  AMQ_ROUTING_KEY_IN = "virustotal"
  AMQ_PREFETCH = 20
  MAX_WORK_PER_WORKER = 20
  attr_reader :ch
  def initialize(logger)
    super
    @env = ENV["ENV"]
    raise ArgumentError, "No ENV variable present" unless @env
    load_config(@env)
    raise ArgumentError, "Configuration for environment #{@env} is invalid" unless check_config
    init_rabbit
  end
  def load_config(env)
    y = YAML.load(File.open('config/rabbitmq.yml'))
    @config = y[env]
  end

  def check_config
    return @config.has_key?("AMQP_HOST")
  end

  def init_rabbit
    begin
      init_rabbit_general
      init_rabbit_inbound
      init_rabbit_outbound
    rescue Bunny::PreconditionFailed => e
      return e
    end
  end  
  def init_rabbit_general
    @logger.debug "Connecting to a RabbitMQ service"
    @conn = Bunny.new(user: @config["AMQP_USER"], password: @config["AMQP_PASS"], host: @config["AMQP_HOST"], heartbeat: 15)
    @conn.start
    @logger.debug "Creating a channel"
    @ch = @conn.create_channel
    @logger.debug "Setting a channel prefetch to #{@config["AMQP_PREFETCH"]} messages"
    @ch.prefetch(@config["AMQP_PREFETCH"])
  end
  def init_rabbit_inbound
    # точка обмена, из которой будем принимать задачи в очередь
    @x_in = @config["AMQP_X_IN"]
    @logger.debug "Accessing an inbound queue"
    @q_in    = @ch.queue(@config["AMQP_QUEUE_IN"], durable: @config["AMQP_QUEUE_IN_DURABLE"])
    @logger.debug "Binding to an inbound exchange"
    @q_in.bind(@x_in, routing_key: @config["AMQP_ROUTING_KEY_IN"])
  end
  def init_rabbit_outbound
    # исходящая точка обмена для результатов
    @x_results = @ch.direct(@config["AMQP_RESULTS_X"], durable: @config["AMQP_RESULTS_X_DURABLE"]) if @config["AMQP_RESULTS_X"]
    # точка для пересылки сообщений
    @x_forward = @ch.direct(@config["AMQP_FORWARD_X"], durable: @config["AMQP_FORWARD_X_DURABLE"]) if @config["AMQP_FORWARD_X"]
  end
  def process_request
    #msgs = fetch_messages
    #reject_messages(msgs)
    #msgs
    fetch_messages
  end
  def publish_result(json_result)
    @x_results.publish(json_result)
  end
  def forward_message(task)
    @logger.info "Publishing to #{@config["AMQP_FORWARD_X"]} message"
    ##{task.task_id} site #{task.website} oldness #{task.oldness}"
    #@x_forward.publish(task.payload_in)
    @x_forward.publish(task, routing_key: @config["AMQP_ROUTING_KEY_IN"])
  end
  def fetch_messages
    # get job from bunny in a non-block manner
  	last_read_ok = true
    results = []
    while last_read_ok
  	  delivery_info, _, payload = @q_in.get(manual_ack: true)
	    last_read_ok = delivery_info ? true : false
      break unless last_read_ok
      results << VtTask.new(self, payload, delivery_info.delivery_tag)
      break if results.count >= MAX_WORK_PER_WORKER
  	end
  	@logger.debug "Got #{results.count} items from a rabbit queue"
	  results
  end
end

