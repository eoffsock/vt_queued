require_relative "lib/rabbit_real.rb"
require_relative "lib/vt_real.rb"
require_relative "scheduled"


class VTWorker < Scheduled
  MAX_OLDNESS = 3600*24*7
  def initialize(_, _, _, vt)
    @vt = vt
    super
  end
  # VTWorker is normally invoked from Scheduler.new
  def too_old?(site)
    @logger.info "Oldness of #{site.website} is #{site.oldness}"
    site.oldness > MAX_OLDNESS
  end
  def actual?(site)
    ! too_old?(site)
  end
  def work(sites)
    # msgs - все результаты
    # found - найдено в VT, есть результат
    # not_found - нужен рескан, нет в кеше
    # actual - актуально (good) 
    # old - нужен рескан, старое
    msgs = @vt.request(name, sites)
    have_result = msgs.select(&:result_present?)
    found = have_result.select(&:found?)
    not_found = have_result - found
    actual = found.select { |m| actual?(m) }
    old = found - actual
    @logger.info "#{name} got #{msgs.count}/#{sites.count} reponses"
    # it is critical to try export results before acking message
    # so if any error occured here, message will not be acked
    msgs.each(&:export_result)
    actual.each(&:accept)
    
    # TODO: just accept now
    #old.each(&:to_rescan)
    old.each(&:accept)

    # TODO: just accept now
    not_found.each(&:accept)
    @logger.info "#{name} got responses: FOUND #{found.count}/#{sites.count}, NOT_FOUND: #{not_found.count}/#{sites.count}"
    @logger.info "#{name} got responses: ACTUAL #{actual.count}/#{sites.count}, OLD: #{old.count}/#{sites.count}"
  end
end

# a clever trick of mine which allows to detect
# if script was executed directly with ruby of pry
if ['(pry)', $PROGRAM_NAME].include?(__FILE__)
  VT_DELAY = 15
  WORKERS = 1

  logger = Logger.new(STDOUT)
  logger.level = Logger::DEBUG
  logger.info "Starting rabbit"
  rabbit = RabbitRequester.new(logger)
  logger.info "Starting Virustotal requester service"
  vt = VirustotalRequester.new(logger)
  logger.info "Starting #{WORKERS} workers with start interval of #{VT_DELAY}"
  pool = WorkerPool.new(logger, VTWorker, VT_DELAY, VT_DELAY, WORKERS, vt)
  workers = pool.workers
  logger.info "Scheduling workers"
  scheduler = RabbitScheduler.new(logger, workers, rabbit)
  logger.info "Initialized scheduler. Now running."
  scheduler.run
end
