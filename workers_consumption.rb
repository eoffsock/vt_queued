require 'nokogiri'
require 'rest-client'


class WorkersConsumption
  LIMIT_PER_DAY = 5760
  def initialize
    @workers = extract_workers_config
  end
  def print_consumption 
    total_limit, total_consumed, total_remaining = 0, 0, 0
    @workers.each do |worker_name, api_key|
      date, consumed = api_key_consumption(api_key)
      remaining = LIMIT_PER_DAY - consumed
      puts "Worker #{worker_name} has consumed #{consumed} @ #{date}"
      puts build_api_key_consumption_url(api_key)
      total_consumed += consumed
      total_remaining += remaining
      total_limit += LIMIT_PER_DAY
    end
    puts "Total consumed: #{total_consumed} / #{total_limit}"
    puts "Total remaining: #{total_remaining} / #{total_limit}"
  end
  def extract_api_key(filename)
    File.read(filename).split('\n').first.strip
  end
  def extract_worker_name(filename)
    filename.match(/config\/([^.]+)/)[1]
  end
  def extract_workers_config
    `ls config/vt_worker*conf`.split("\n").map{ |f| [extract_worker_name(f), extract_api_key(f)] }.to_h
  end
  def build_api_key_consumption_url(api_key)
    "https://www.virustotal.com/ru/user/"+api_key+"/apikey/consumption/"
  end
  def get_page(url)
    RestClient.get url
  end
  def parse_page_for_consumption(url)
    page = get_page(url)
    dom = Nokogiri::HTML(page)
    date = dom.css('#consumption-list > tr:nth-child(1) > td:nth-child(1)').first.content
    consumed = dom.css('#consumption-list > tr:nth-child(1) > td:nth-child(2) > span').first.content.to_i
    [date, consumed]
  end
  def api_key_consumption(api_key)
    url = build_api_key_consumption_url(api_key)
    parse_page_for_consumption(url)
  end
end

wc = WorkersConsumption.new
wc.print_consumption

