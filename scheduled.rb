#!/usr/bin/env ruby
# encoding: utf-8

require "bunny"

class Scheduled
  attr_accessor :name
  # Provides ability to #run job_portion trough #work
  # in even intervals defined by @period
  def initialize(logger, period, name, *args)
    @name = name
    @logger = logger
    # never invoked yet
    @last_run = Time.at(0)
    @period = period
  end
  def next_run_at
    @last_run + @period
  end
  def next_run_in
    result = next_run_at - Time.now
    return 0 if result < 0
    result
  end
  def can_run?
    next_run_in <= 0
  end
  def update_last_run
    @last_run = Time.now
  end
  def wait
    unless can_run?
      puts "Waiting for #{next_run_in}"
      sleep next_run_in
    end
  end
  def run(job_portion)
    wait
    update_last_run
    work(job_portion)
  end
  def work(job_portion)
    # overload this method to do something useful
  end
end

class Scheduler
  def initialize(logger, scheduled, *args)
    @scheduled = scheduled
    @logger = logger
  end
  def which_next
    @scheduled.sort_by {|w| w.next_run_in}.first
  end
  def run_next
    which_next.run
  end
  def stop
    puts "Stopping a scheduler"
    @running = false
  end
  def run
    @running = true
    while @running do
      next_run = which_next.next_run_in
      @logger.debug "Schedule: Next scheduled is #{which_next.name} and it is scheduled to run in #{which_next.next_run_in} seconds"
      sleep next_run if next_run > 0
      job_portion = get_jobs_block
      @logger.info "Schedule: Got job portion of #{job_portion.count} items from source"
      start = Time.now
      which_next.run(job_portion)
      diff = Time.now - start
      @logger.debug "Schedule: #{which_next.name} is done in #{diff} seconds"
    end
  end
  def get_jobs_block
    # overload this
    raise "undefined!!"
  end
end

class RabbitScheduler < Scheduler
  AMQP_POLL_INTERVAL = 3
  # if empty
  def initialize(logger, scheduled, rabbit)
    @rabbit = rabbit
    super
  end
  def get_jobs
    # assuming this is a non-block method which can return empty set
    new_jobs = @rabbit.get_job_from_queue
    @logger.info "Schedule: There were #{new_jobs.count} job items from rabbit."
    new_jobs
  end
  def get_jobs_block
  	# it is polling for jobs with non blocking #get_jobs at AMQP_POLL_INTERVAL
    while true do
      job = get_jobs
      if job.count > 0
        return job
      end
      @logger.debug "Schedule: BLOCK. No jobs yet..."
      sleep AMQP_POLL_INTERVAL
    end
  end
end

class WorkerPool
  def initialize(logger, klass, param, period, count, vt)
    @vt = vt
    @logger = logger
    @workers = []
    count.times do |n|
      @logger.debug "Pool: starting worker #{n}..."
      workers << klass.new(@logger, param, "#{klass}:#{n}", vt)
      # is this a last one?
      if n == (count - 1)
        @logger.debug "Pool: OK. That was the last one"
      else
        @logger.debug "Pool: OK. Now waiting for #{period} seconds before next worker start"
        sleep period
      end
    end
  end
  def workers
    @workers
  end
end
