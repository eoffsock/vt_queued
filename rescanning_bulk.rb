require 'logger'
require_relative 'rescanning_single'

class RescannerWorker
  # ресканит джобы
  attr_reader :name
  MAX_ATTEMPTS = 5
  def initialize(vt, logger)
    @vt = vt
    @logger = logger
    @name = "RescannerWorker1"    
  end
  def work(sites, attempt = 0)
    if attempt > MAX_ATTEMPTS
      @logger.info "#{name} reached maximum attempts count #{attempt} and still has #{sites.count} jobs"
      return sites
    end
    @logger.info "#{name} making attempt #{attempt} with #{sites.count} sites"
    msgs = @vt.request(self.name, sites)
    good = msgs.select { |m| m.good? }
    bad = msgs.select { |m| m.bad? }
    if bad.count == 0
      @logger.info "#{name} all sites got processed on attempt #{attempt}"
      return [] 
    end
    @logger.info "#{name} got #{good.count}/#{msgs.count} reponses"
    # it is critical to try export results before acking message
    # so if any error occured here, message will not be acked
    good.each(&:export_result)
    good.each(&:accept)
    @logger.info "#{name} got responses: GOOD #{good.count}/#{sites.count}, BAD: #{bad.count}/#{sites.count}"
    remaining_bad = work(bad, attempt + 1)
    return [] if remaining_bad.empty?
    remaining_bad.each(&:export_result)
    remaining_bad.each(&:reject)
    []
  end
end

class FakeVirustotalRescanningService
  MAX_WORK_PER_REQUEST = 20
  attr_reader :api_requests
  def initialize(logger)
    @logger = logger
    @api_requests = 0
    @bulk_api_requests = 0
  end
  def request(name, jobs)
    raise TooMuchWork if jobs.count > MAX_WORK_PER_REQUEST
    jobs.each do |j|
      next if rand > 0.2
      j.vt_result = 'It is a result from FakeVT. It is not empty'
    end
  end
end

class TooMuchWork < StandardError
end

class VTJob
  attr_accessor :vt_result
  attr_reader :website, :status
  def initialize(website, logger)
    @logger = logger
    @website = website
    @vt_result = ''
    @rabbit_status = []
  end
  def export_result
    @logger.info "Exported #{@website}"
    @status = "progress"
  end
  def accept
    @logger.info "Accepted #{@website}"
    @status = "finished"
  end
  def reject
    @logger.info "Rejected #{@website}"
    @status = "error"
  end
  def has_result?
    ! @vt_result.empty?
  end
  def finished?
    @status == "finished"
  end
  def good?
    ! bad?
  end
  def bad?
    return true unless has_result?
    false
  end
  def oldness
    return nil unless has_result?
    vt_result
  end
end

class JobProvider
  DB_FILE = 'sample/websites_all.txt' 
  BULK_SIZE = 20
  def initialize(logger)
    @logger = logger
    @db = File.open(DB_FILE).each_line.lazy
    @last_job_batch = []
  end
  def get_bulk_sites
    @db.first(BULK_SIZE).map(&:strip)
  end
  def get_bulk_jobs
    @last_job_batch = get_bulk_sites.map { |site| VTJob.new(site, @logger) }
  end
  def last_bulk_stats
    @last_job_batch.sort {|x, y| x.status <=> y.status}.chunk {|j| j.status}.map {|status, j| [status, j.count]}.to_h
  end
end


# a clever trick of mine which allows to detect
# if script was executed directly with ruby of pry
if ['(pry)', $PROGRAM_NAME].include?(__FILE__)
  logger = Logger.new STDOUT
  logger.level = Logger::DEBUG

  vt = FakeVirustotalRescanningService.new(logger)

  job_provider = JobProvider.new(logger)
  jobs = job_provider.get_bulk_jobs

  worker = RescannerWorker.new(vt, logger)
  worker.work(jobs)
  r = job_provider.last_bulk_stats

  binding.pry
end
