require_relative "lib/rabbit_fake.rb"
require_relative "lib/vt_fake.rb"
require_relative "scheduled"


class VTWorker < Scheduled
  def work(sites)
	#request but then do nothing with it
    $vt.request(name, sites)
  end
end

VT_DELAY = 3
WORKERS = 3

$logger = Logger.new(STDOUT)

$logger.info "Starting rabbit"
$rabbit = RabbitMocker.new
$logger.info "Starting Virustotal requester service"
$vt = VirustotalMocker.new
$logger.info "Starting #{WORKERS} workers with start interval of #{VT_DELAY}"
pool = WorkerPool.new(VTWorker, VT_DELAY, VT_DELAY, WORKERS)
workers = pool.workers
$logger.info "Scheduling workers"
scheduler = RabbitScheduler.new(workers, $rabbit)
$logger.info "Initialized."
scheduler.run
