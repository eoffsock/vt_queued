$LOAD_PATH << 'lib'

require 'rescan_asker_bulk'
require 'ruby-progressbar'

class BarBuilder
  BAR_FORMAT = "%t %a %e %r rows/sec %w"
  def self.create(total, title)
    ProgressBar.create(total: total, start_at: 0, throttle_rate: 0.5, title: title, format: BAR_FORMAT)
  end
end

class VirustotalRescanAskerBulkFromFile < VirustotalRescanAskerBulk
  def initialize *args
    @api_key, filename_in = args
    clean_filename = filename_in.split('/').last
    @file_in="input/#{clean_filename}"
    @file_out="log/#{clean_filename}"
    total_in = `wc -l #{@file_in}`.split(' ').first.to_i
    @bar = BarBuilder.create(total_in, 'Processing')
    super
  end
  def process_good_item l
    url = l["url"]
    permalink = l["permalink"]
    response_code = l["response_code"]
    @file_out_opened << "#{url};#{permalink};#{response_code}\n" 
  end
  def process_bad_item l
    url = l["url"]
    response_code = l["response_code"]
    $log.warn "#{url} has response_code #{response_code}"
    @file_out_opened << "#{url};ERROR;#{response_code}\n" 
  end
  def work
    File.open(@file_out, 'w') do |file_out|
      @file_out_opened = file_out
      File.open(@file_in) do |file_in|
        $log.debug "Initialized"
        iter = file_in.each_line
        while portion = iter.first(MAX_PER_REQUEST)
          process_portion(portion)
          @bar.progress += portion.size
        end
      end 
    end
  end
end

if __FILE__ == $0 || __FILE__ == '(pry)'
  
  puts ARGV.inspect
  exit unless ARGV.size == 2
  api_key = ARGV[0]
  filename_in = ARGV[1]
  log_filename = "log/just_rescan.log"
  #log_filename = STDOUT
  $log = Logger.new(log_filename)
  $log.level = Logger::DEBUG
  vtra = VirustotalRescanAskerBulkFromFile.new(api_key, filename_in)
  vtra.work
end
