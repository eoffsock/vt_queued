class App < Sinatra::Base

  get "/" do
    "Please use an API."
  end

  get "/scan/:website" do |website|
    jm = JobManager.new
    return jm.insert(website).to_s
  end

  get "/result/:id" do |id|
    jm = JobManager.new
    jm.job(id).inspect
  end

  #TODO: remove this from production
  get "/console" do
    binding.pry
  end

  #TODO: remove this from production
  get "/params" do
    params.inspect
  end
  post "/params" do
    params.inspect
  end

end
