require_relative 'spec_helper'
require_relative '../lib/vt_tester'
require 'pry'

describe VirustotalRequester do
  before(:all) do
    @vtr = VirustotalRequester.new
    @ch = FakeChannel.new
    $logger = Logger.new(STDOUT)
    $logger.level=Logger::FATAL
  end
  it "#normalize_proto" do
    expect(@vtr.normalize_proto('www.ya.ru/')).to eq "http://www.ya.ru/"
    expect(@vtr.normalize_proto('http://www.ya.ru/')).to eq "http://www.ya.ru/"
  end
  it "should test www.ya.ru successfully" do
    website = "www.ya.ru"
    task_id = 0
    payload_in = {"website": website, "task_id": task_id}.to_json
    info = FakeDelivery.new(website)
    msg = RabbitMessageTest.new(payload_in, info, @ch)
    @vtr.process_request([msg])

    expect(msg.status).to eq "success"
    expect(msg.vt_result).to have_key(:hidden)
    expect(msg.vt_result).to have_key(:vulnerable)
    expect(msg.vt_result[:vulnerable]).to be false
    expect(msg.vt_result[:hidden]).to have_key(:scan_date)
  end
end
