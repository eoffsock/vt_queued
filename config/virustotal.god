(1..4).each do |num|
  God.watch do |w|
    w.name = "virustotal_#{num}"
    w.group = "virustotal"
    w.uid = 'ilya.ruby'
    w.gid = 'ilya.ruby'
    w.dir = "/home/ilya.ruby/virustotal/current"
    w.env = { 'ENV' => 'db02',
              'API_KEY' => File.read("#{w.dir}/config/vt_worker#{num}.conf").strip }
    w.behavior(:clean_pid_file)
    w.start = "/home/ilya.ruby/.rvm/bin/rvm ruby-2.2@virustotal exec bundle exec ruby vt_queue_real.rb"
    w.log = "#{w.dir}/log/#{w.name}_god.log"
    puts w.log
    w.keepalive(:memory_max => 128.megabytes,
                :cpu_max => 50.percent)
  end
end
