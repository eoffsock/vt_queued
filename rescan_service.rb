$LOAD_PATH << 'lib'

require 'rescan_asker_bulk'
require_relative "lib/rabbit_real.rb"

class VirustotalRescanAskerBulkFromRabbit < VirustotalRescanAskerBulk
  def initialize api_key
    $log.debug "Initializing"
    @api_key = api_key
    @rabbit = RabbitRequester.new($log)
    super
  end
  def process_good_item l
    forward_item l
    l.accept
  end
  def process_bad_item l
    forward_item l
    l.accept
  end
  def forward_item l
    @rabbit.forward_message recreate_task_from_response(l)
  end
  def recreate_task_from_response l
    { "website": l.website,
      "task_id": l.task_id
    }.to_json
  end
  def work
    $log.debug "Initialized"
    while true
      portion = @rabbit.fetch_messages
      if portion.empty?
        $log.debug "Empty portion"
        sleep 3
      else
        $log.info "Have portion: #{portion.size}"
        process_portion(portion)
        sleep 15
      end
    end
  end
end

if __FILE__ == $0 || $0 == 'pry'
  
  puts ARGV.size
  puts ARGV.inspect
  unless ARGV.size == 1
    puts "Provide an API_KEY"
    exit 1
  end
  api_key = ARGV[0]
  #log_filename = "log/rescan_service.log"
  log_filename = STDOUT
  $log = Logger.new(log_filename)
  $log.level = Logger::DEBUG
  $log.debug "Starting"
  vtra = VirustotalRescanAskerBulkFromRabbit.new(api_key)
  vtra.work
end
